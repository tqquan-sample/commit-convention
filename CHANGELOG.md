# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2020-12-03)


### Features

* print greeting message ([83d75d0](https://gitlab.com/tqquan-sample/commit-convention/commit/83d75d0418505f5db870adcf535c49d2638e084c))


### Bug Fixes

* update message text ([dd6fbd0](https://gitlab.com/tqquan-sample/commit-convention/commit/dd6fbd092da6d68533697742479f4ff3f130caa4))


### Docs

* add README ([9573112](https://gitlab.com/tqquan-sample/commit-convention/commit/9573112c041a69f41ddaf8f334337a1de8f17172))


### Styling

* correct style for index.js ([e25323d](https://gitlab.com/tqquan-sample/commit-convention/commit/e25323d11dcc67e2920eb2a02bf8927316942bec))


### Others

* disable emoji for commit ([84b66bc](https://gitlab.com/tqquan-sample/commit-convention/commit/84b66bced660ab257d9170e902dd52dc23676f72))
* init project ([11a16f2](https://gitlab.com/tqquan-sample/commit-convention/commit/11a16f2a5c0c93e8489768b01fb5dcc3f6596cc4))
